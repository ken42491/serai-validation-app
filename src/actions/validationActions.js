import * as types from '../constants/actionTypes';
import axios from 'axios'
import { SERVER_URL } from '../config';

// redux middleware
export function validatePhone(settings) {
  return async function (dispatch) {
    // Start Verification - set loading to true
    let loading = 'start'
    dispatch({ type: types.VALIDATION_LOADING, loading });

    try {
      let { country, phoneNumber } = settings
      // Prepare to call API
      let url = `${SERVER_URL}/v1/verification/phone-verify?number=${phoneNumber}&country_code=${country}`
      let result = await axios.get(url)

      // If API has returned result
      if (result && result.status == 200) {
        let data = result.data
        dispatch({ type: types.VALIDATE_PHONE, data });
      }

      // Finished verification
      loading = 'stop'
      dispatch({ type: types.VALIDATION_LOADING, loading });  
    } catch (error) {
      // Error - finish verification
      loading = 'stop'
      dispatch({ type: types.VALIDATION_LOADING, loading });      
    }
  }
}

export function validateEmail(email) {
  return async function (dispatch) {
    // Start Verification - set loading to true
    let loading = 'start'
    dispatch({ type: types.VALIDATION_LOADING, loading });

    try {
      // Prepare to call API
      let url = `${SERVER_URL}/v1/verification/email-verify?email=${email}`
      let result = await axios.get(url)

      // If API has returned result
      if (result && result.status == 200) {
        let data = result.data
        dispatch({ type: types.VALIDATE_EMAIL, data });
      }

      // Finished verification
      loading = 'stop'
      dispatch({ type: types.VALIDATION_LOADING, loading });  
    } catch (error) {
      // Error - finish verification
      loading = 'stop'
      dispatch({ type: types.VALIDATION_LOADING, loading });      
    }
  }
}