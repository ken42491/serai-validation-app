import initialState from './initialState';
import objectAssign from 'object-assign';

// IMPORTANT: Note that with Redux, state should NEVER be changed.
// State is considered immutable. Instead,
// create a copy of the state passed and set new values on the copy.
// Note that I'm using Object.assign to create a copy of current state
// and update values on the copy.
export default function validationReducer(state = initialState.validationProps, action) {
  let newState;

  switch (action.type) {

    case 'VALIDATE_PHONE':
      newState = objectAssign({}, state);
      let { data } = action

      // Save to Latest Validation
      newState.latestValidation = data

      // Append to validatedPhoneNumber Data
      if (data && data.resultStatus && data.data.result.valid) {
        
        // Check Duplication
        let i = 0
        for (i in newState.validatedPhoneNumber) {
          let item = newState.validatedPhoneNumber[i]
          let strItem = JSON.stringify(item)
          let strData = JSON.stringify(data)
          if (strItem == strData) break
          i++
          
          if (i == newState.validatedPhoneNumber.length) {
            newState.validatedPhoneNumber.push(data)
          }
        }

        if (newState.validatedPhoneNumber.length == 0) {
          newState.validatedPhoneNumber.push(data)
        }
      }

      // Append to histories
      newState.histories.push(data)

      return newState

    case 'VALIDATE_EMAIL':
      newState = objectAssign({}, state);

      // Append to histories
      newState.histories.push(action.data)

      return newState

    case 'VALIDATION_LOADING':
      newState = objectAssign({}, state);
      newState.validateStatus = action.loading;
      return newState

    default:
      return state;
  }
}
