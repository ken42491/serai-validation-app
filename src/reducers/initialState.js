export default {
  
  // Validation
  validationProps: {
    validatedPhoneNumber: [],
    validateLoading: false,
    validateStatus: 'end',
    latestValidation: {},
    histories: []
  }
  
};
