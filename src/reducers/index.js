import { combineReducers } from 'redux';
import validationProps from './validationReducer';
import { connectRouter } from 'connected-react-router'

const rootReducer = history => combineReducers({
  router: connectRouter(history),
  validationProps
});

export default rootReducer;
