// Centralized propType definitions
import { shape, number, bool, string, oneOfType, array } from 'prop-types';

export const savings = shape({
  monthly: oneOfType([number,string]),
  annual: oneOfType([number,string]),
  threeYear: oneOfType([number,string]),
});

export const validationFormTypes = shape({
  countryCode: string,
  phoneNumber: oneOfType([number,string]),
  disableSelectValidated: bool,
  validatedPhoneNumber: array,
  loadingVerification: bool
})
