import React from 'react';
import { Form, Icon, Input, Button, Select } from 'antd';

// Types
import { func } from 'prop-types';
import { validationFormTypes } from '../../../types';

// Testing Data
import countries from './countries.json'

const { Option, OptGroup } = Select;

const ValidationForm = ({
  onValidate, 
  onSelectValidatedNumber, 
  validatedPhoneNumber, 
  disableSelectValidated,
  onSelectCountryCode,
  initForm,
  loadingVerification,
  onTypeNumber,
  onFilter,
  onTypeEmail,
  email
}) => (
  <div>
    <Form className="validation-form">

      {/* Saved Validated Number */}
      <Form.Item>
        <Select 
          onChange={onSelectValidatedNumber} 
          disabled={disableSelectValidated}
          placeholder={validatedPhoneNumber && validatedPhoneNumber.length > 0 ? 'Select your previous validated phone number' : 'Please validate at least one phone number first'}
        >
          <OptGroup label="Validated Phone Number">
            { 
              validatedPhoneNumber.map((item, index) => {
                let { data } = item
                let { result } = data
                return (<Option value={JSON.stringify(result)} key={index}>{`( ${result.country_prefix} ) ${result.local_format}`}</Option>)
              })
            }
          </OptGroup>
        </Select>
      </Form.Item>

      {/* Country Code */}
      <Form.Item>
        <Select 
          showSearch
          value={initForm.country}
          onChange={onSelectCountryCode}
          defaultValue={initForm.country}
          onFilter={onFilter}
        >
          <OptGroup label="Select Country Code">
            {/* Getting different countries */}
            { Object.entries(countries).map(([country, {country_name, dialling_code}]) => {
              return (
                <Option 
                  key={country} 
                  value={country}
                >
                  {`${country_name} ( ${dialling_code} )` || 'N/A'}
                </Option>)
            }) }
          </OptGroup>
        </Select>
      </Form.Item>

      {/* Phone Number */}
      <Form.Item>
        <Input 
          onChange={onTypeNumber}
          type="text" 
          pattern="[0-9]*" 
          suffix={<Icon type="phone" style={{ color: 'rgba(0,0,0,.25)' }} />}
          placeholder="Phone Number" 
          value={initForm.phoneNumber}
        />
      </Form.Item>

      {/* Email */}
      <Form.Item>
        <Input 
          onChange={onTypeEmail}
          type="text"
          suffix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
          placeholder="Email" 
          value={email}
        />
      </Form.Item>

      {/* Validation Button */}
      <Form.Item>      
        <Button type="primary" onClick={onValidate} className="validation-button" loading={loadingVerification}>
            Validate
        </Button>
      </Form.Item>
    </Form>
  </div>
);

ValidationForm.propTypes = {
  initForm: validationFormTypes.isRequired,
  onValidate: func.isRequired,
  onSelectValidatedNumber: func.isRequired,
  onSelectCountryCode: func.isRequired,
  onTypeNumber: func.isRequired
};

export default ValidationForm;
