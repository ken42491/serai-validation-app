import React from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { PageHeader, message, Alert } from 'antd';

// Components
import ValidationForm from './Form'
import ValidationTable from './Table'

// Redux
import * as actions from '../../../actions/validationActions';

// Styles
import '../../../styles/validation.scss'

export class ValidationLanding extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      initForm: {
        country: 'HK',
        phoneNumber: null
      },
      loadingVerification: false,
      infoAlert: {
        type: 'info',
        message: 'Feel free to validate any phone number or email'
      },
      validatedPhoneNumber: [],
      histories: [],
      email: ""
    };
  }

  componentDidMount () {
    let { validationProps } = this.props
    let { validateLoading = false } = validationProps

    // set loading verification to false at init
    this.setState({ loadingVerification: validateLoading })
  }

  componentWillReceiveProps (nextProps) {
    let { validationProps } = nextProps

    // Change loading status
    if (validationProps.validateStatus) {
      let { histories, validateStatus, validatedPhoneNumber } = validationProps
      if (validateStatus == 'start') {
        this.setState({loadingVerification: true})
      } else {
        // Clear data
        let initForm = {
          country: 'HK',
          phoneNumber: null
        }
        this.setState({loadingVerification: false, validatedPhoneNumber, histories, initForm})
      }
    }

    // Show latest validation message
    if (validationProps.latestValidation.hasOwnProperty('data')) {
      let { data } = validationProps.latestValidation
      if (data && data.result) {

        let { result } = data
        // Set message to success or error
        if (validationProps.latestValidation.resultStatus && result.valid) {
          let newInfoAlert = { type: 'success', message: `Your phone number ${result.number} is verified` }
          this.setState({ infoAlert: newInfoAlert })
        } else {
          let newInfoAlert = { type: 'error', message: `Your phone number ${result.number} cannot be verified` }
          this.setState({ infoAlert: newInfoAlert })
        }

      }
    }
  }

  // Validate Phone Number
  onValidate = () => {
    let { initForm, email } = this.state
    let { country, phoneNumber = ''} = initForm
    let { actions } = this.props
 
    // Checking the form to see if user has entered all details
    if (!country || (!phoneNumber) || phoneNumber.length == 0) {
      message.warning('Phone will not be validated');
    } else {
      actions.validatePhone(initForm)
    }

    // Checking the form to see if user has entered email
    if (!email) {
      message.warning('Email will not be validated');
    } else {
      actions.validateEmail(email)
    }
  }

  // Select Validated Phone Number
  onSelectValidatedNumber = (value) => {
    let item = JSON.parse(value)
    let newForm = {
      country: item.country_code,
      phoneNumber: parseInt(item.local_format)
    }
    this.onSelectCountryCode(item.country_code)
    this.setState({ initForm: newForm })
  }

  // Select Country Code
  onSelectCountryCode = (value) => {
    // Save country to the form for validation
    let { initForm } = this.state
    initForm.country = value
    this.setState({ initForm })
  }

  // On Filter
  onFilter = (input, option) => {
    return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
  }

  // Type in phone number & checking
  onTypeNumber = (e) => {
    let newVal = e && ( e.target.validity.valid ) ? e.target.value : 'invalid'

    // If user types in invalid character, will return
    if (newVal === 'invalid') {
      return
    }

    // Save phone number to the form for validation
    let { initForm } = this.state
    initForm.phoneNumber = newVal
    this.setState({ initForm })
  }

  // Type in email & checking 
  onTypeEmail = (e) => {
    let newVal = e && ( e.target.validity.valid ) ? e.target.value : 'invalid'
    // If user types in invalid character, will return
    if (newVal === 'invalid') {
      return
    }

    // Save email to the form for validation
    this.setState({ email: newVal })
  }

  render() {
    let { initForm, loadingVerification, infoAlert, validatedPhoneNumber, histories, email } = this.state

    return (
      <div className='validation-landing'>
        {/* Title */}
        <PageHeader title="Serai Validation" className='title' />

        {/* Form */}
        <ValidationForm 
          onValidate={this.onValidate}
          onSelectValidatedNumber={this.onSelectValidatedNumber}
          validatedPhoneNumber={validatedPhoneNumber}
          disableSelectValidated={validatedPhoneNumber && !validatedPhoneNumber.length > 0}
          onSelectCountryCode={this.onSelectCountryCode}
          initForm={initForm}
          loadingVerification={loadingVerification}
          onTypeNumber={this.onTypeNumber}
          onFilter={this.onFilter}
          onTypeEmail={this.onTypeEmail}
          email={email}
        />

        {/* Information Box to show information or error */}
        <Alert type={infoAlert.type} message={infoAlert.message} showIcon className='alert' />

        {/* Table */}
        <ValidationTable histories={histories} />
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    validationProps: state.validationProps
  };
}

function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ValidationLanding);
