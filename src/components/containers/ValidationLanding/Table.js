import React from 'react';
import { Table, Tag, Icon, Descriptions } from 'antd';

const expandedRowRender = (data) => {
  return (
    <div>
      { 
        data.resultDesc == 'phone_is_verified' ?
        <Descriptions title="Phone Verification Information" bordered>
          <Descriptions.Item label="International Format">{data.data.result.international_format}</Descriptions.Item>
          <Descriptions.Item label="Local Format">{data.data.result.local_format}</Descriptions.Item>
          <Descriptions.Item label="Country Prefix">{data.data.result.country_prefix}</Descriptions.Item>
          <Descriptions.Item label="Line Type">{data.data.result.line_type}</Descriptions.Item>
          <Descriptions.Item label="Country Name">{data.data.result.country_name}</Descriptions.Item>
          <Descriptions.Item label="Location">{data.data.result.location}</Descriptions.Item>
          <Descriptions.Item label="Carrier">{data.data.result.carrier}</Descriptions.Item>
        </Descriptions>
        :
        <Descriptions title="Email Verification Information" bordered>
          <Descriptions.Item label="User">{data.data.result.user}</Descriptions.Item>
          <Descriptions.Item label="Domain">{data.data.result.domain}</Descriptions.Item>
          <Descriptions.Item label="Did you mean">{data.data.result.did_you_mean}</Descriptions.Item>
          <Descriptions.Item label="Score">{data.data.result.score}</Descriptions.Item>
          <Descriptions.Item label="Format Valid">{ data.data.result.format_valid ? <Icon type="check" style={{color: 'green'}} /> : <Icon type="close" style={{color: 'red'}}/>}</Descriptions.Item>
          <Descriptions.Item label="Role">{ data.data.result.role ? <Icon type="check" style={{color: 'green'}} /> : <Icon type="close" style={{color: 'red'}}/>}</Descriptions.Item>
          <Descriptions.Item label="MX found">{ data.data.result.mx_found ? <Icon type="check" style={{color: 'green'}} /> : <Icon type="close" style={{color: 'red'}}/>}</Descriptions.Item>
          <Descriptions.Item label="SMTP Check">{ data.data.result.smtp_check ? <Icon type="check" style={{color: 'green'}} /> : <Icon type="close" style={{color: 'red'}}/>}</Descriptions.Item>
          <Descriptions.Item label="Catch All">{ data.data.result.catch_all ? <Icon type="check" style={{color: 'green'}} /> : <Icon type="close" style={{color: 'red'}}/>}</Descriptions.Item>
          <Descriptions.Item label="Free">{ data.data.result.free ? <Icon type="check" style={{color: 'green'}} /> : <Icon type="close" style={{color: 'red'}}/>}</Descriptions.Item>
          <Descriptions.Item label="Disposable">{ data.data.result.disposable ? <Icon type="check" style={{color: 'green'}} /> : <Icon type="close" style={{color: 'red'}}/>}</Descriptions.Item>
        </Descriptions>
      }
    </div>
  )
}

const columns = [
  {
    title: 'Type',
    key: 'type',
    dataIndex: 'resultDesc',
    render: type => (
      <span>
        <Tag color={type == 'email_is_verified' ? 'geekblue' : 'green'}>
          {type == 'email_is_verified' ? 'EMAIL' : 'PHONE'}
        </Tag>
      </span>
    ),
  },
  {
    title: 'Phone Validated',
    key: 'valid',
    dataIndex: 'data.result.valid',
    render: valid => (
      <span>
        { valid ? <Icon type="check" style={{color: 'green'}} /> : <Icon type="close" style={{color: 'red'}}/>}
      </span>
    ),
  },
  {
    title: 'Country Code',
    dataIndex: 'data.result.country_code',
    key: 'country_code',
  },
  {
    title: 'Phone Number',
    dataIndex: 'data.result.number',
    key: 'number',
  },
  {
    title: 'Email',
    dataIndex: 'data.result.email',
    key: 'email',
  }
];

const ValidationTable = ({
  histories
}) => (
  <div className='table'>
    <Table 
      className="components-table-demo-nested" 
      dataSource={histories} 
      expandedRowRender={expandedRowRender} 
      columns={columns} 
    />
  </div>
);

export default ValidationTable;
